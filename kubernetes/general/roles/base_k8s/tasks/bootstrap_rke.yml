# - name: debug OS facts
#   debug:
#     var: "{{ ansible_ssh_private_key_file }}"

- name: Wait for apt to unlock | Debian All
  ansible.builtin.shell: while sudo fuser /var/lib/dpkg/lock >/dev/null 2>&1; do sleep 5; done;

- name: Ensure packages are absent from system | Debian All
  ansible.builtin.apt:
    name: "{{ item }}"
    state: absent
  with_items:
  - "{{ absent_packages }}"

- name: Set authorized key for the current user
  ansible.posix.authorized_key:
    user: "{{ ansible_user }}"
    key: "{{ lookup('file', ssh_access_key | default('~/.ssh/id_rsa.pub')) }}"
    state: present
    # exclusive: true

- name: Prevent packages to be installed/updated | Debian All
  ansible.builtin.dpkg_selections:
    selection: hold
    name: "{{ item }}"
  with_items:
  - "{{ excluded_packages }}"
  when: excluded_packages is defined and excluded_packages is not none and excluded_packages|length > 0


- name: Generate /etc/hosts file | Debian All
  ansible.builtin.template:
    backup: true
    src: hosts.j2
    dest: /etc/hosts
    mode: "0o644"

- name: Modprobe enable modules | Debian All
  community.general.modprobe:
    name: "{{ item.name }}"
    state: present
  with_items:
  - {name: "br_netfilter",}

- name: Update sysctl | Debian All
  ansible.posix.sysctl:
    name: "{{ item.name }}"
    value: "{{ item.value }}"
    sysctl_file: /etc/sysctl.d/kubernetes.conf
    sysctl_set: true
    state: "{{ item.state }}"
    reload: true
  with_items:
  - {name: "net.bridge.bridge-nf-call-iptables", value: "1", state: "present"}
  - {name: "net.ipv4.ip_forward", value: "1", state: "present"}
  - {name: "net.ipv4.tcp_congestion_control", value: "bbr", state: "present"}
  # /proc/sys/net/ipv4/tcp_fin_timeout (default 60)
  - {name: "net.ipv4.tcp_fin_timeout", value: "35", state: "present"}
  - {name: "fs.file-max", value: "262144", state: "present",}

- name: Ensure base packages are installed | Debian All
  ansible.builtin.apt:
    name: "{{ required_k8s_packages }}"
    update_cache: true
    cache_valid_time: 86400
  retries: 5
  delay: 10


- name: Add Docker official GPG key | Debian All
  ansible.builtin.apt_key:
    url: "{{ item }}"
    state: present
  with_items:
  - https://download.docker.com/linux/debian/gpg
  # when:
  #   - (ansible_facts['ansible_virtualization_role'] == "host" )

- name: Add Docker apt Repository | Debian All
  ansible.builtin.apt_repository:
    repo: "{{ item }}"
    state: present
  with_items:
  - deb https://download.docker.com/linux/debian {{ ansible_distribution_release }} stable
  when: ansible_distribution_release == "bullseye" or ansible_distribution_release == "bookworm"

- name: Add Docker pinning | Debian All
  ansible.builtin.copy:
    src: docker.prefs
    dest: /etc/apt/preferences.d/99-docker-ce
    mode: "0o644"

- name: Waits debian repository to become available | Debian All
  ansible.builtin.wait_for:
    host: debian.ipacct.com
    port: 80
    delay: 2
    sleep: 3
    state: started

- name: Install Docker | Debian All
  ansible.builtin.apt:
    name: "{{ required_docker_packages }}"
    update_cache: true
    cache_valid_time: 86400
    allow_downgrade: true
  retries: 5
  delay: 10

- name: Install Longhorn Deps | Debian All
  ansible.builtin.apt:
    name: "{{ required_longhorn_packages }}"
    update_cache: true
    cache_valid_time: 86400
  notify: Enable_services
  retries: 5
  delay: 10

- name: Create config directories | Debian All
  ansible.builtin.file:
    path: /root/.docker/
    state: directory
    mode: "0o750"

- name: Copy docker daemon config | Debian All
  ansible.builtin.copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    mode: "{{ item.mode }}"
  notify: Reload_dockerd
  with_items:
  - {src: "daemon.json", dest: "/etc/docker/daemon.json", mode: "0o644"}
  # - {src: "config.json", dest: "/root/.docker/config.json", mode: "0o644"}
  - {src: "docker-container", dest: "/etc/logrotate.d/docker-container", mode: "0o644",}


- name: Security limits | Debian All
  community.general.pam_limits:
    domain: "{{ item.domain }}"
    limit_type: "{{ item.type }}"
    limit_item: "{{ item.item }}"
    value: "{{ item.value }}"
  with_items:
  - {domain: "*", type: "hard", item: "nofile", value: "39693561"}
  - {domain: "*", type: "hard", item: "nproc", value: "39693561"}
  - {domain: "*", type: "soft", item: "memlock", value: unlimited}
  - {domain: "*", type: "hard", item: "memlock", value: unlimited,}

- name: Generate /etc/chrony/chrony.conf file | NTP Client Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: chrony.j2
    dest: /etc/chrony/chrony.conf
    mode: "0o655"
  notify: Restart_chrony
# - name: Disable Swap in fstab | Debian All
#   lineinfile:
#     path: /etc/fstab
#     regexp: '^/dev/pve/swap'
#     line: '#/dev/pve/swap none swap sw 0 0'
#   notify: swapoff
