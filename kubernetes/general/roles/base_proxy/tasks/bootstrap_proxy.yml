---

# - name: Populate OS facts
#   setup:
#   register: setup

# - name: Debug OS facts
#   debug:
#     var: setup

- name: Ensure Repository Cache is up to date | Debian All
  ansible.builtin.apt:
    update_cache: true

- name: Ensure packages are absent from system | Debian All
  ansible.builtin.apt:
    name: "{{ item }}"
    state: absent
  with_items:
    - "{{ absent_packages }}"

# - name: Prevent packages to be installed/updated
#   ansible.builtin.dpkg_selections:
#     selection: hold
#     name: "{{ item }}"
#   with_items:
#     - "{{ excluded_packages }}"
#   when: excluded_packages is defined

- name: Ensure OS is up to date | Debian All
  ansible.builtin.apt:
    name: "*"
    state: latest
  when:
    - ( hostvars[inventory_hostname].os_update == 'true'  )

- name: Ensure the required packages are installed | Debian All
  ansible.builtin.apt:
    name: "{{ item }}"
    state: present
    update_cache: true
  with_items:
    - "{{ base_packages }}"
    - "{{ service_packages }}"
  notify: Enable_services

- name: Update sysctl | Debian All
  ansible.posix.sysctl:
    name: "{{ item.name }}"
    value: "{{ item.value }}"
    sysctl_file: /etc/sysctl.d/kubernetes.conf
    sysctl_set: true
    state: "{{ item.state }}"
    reload: true
  with_items:
    - { name: 'net.ipv4.tcp_congestion_control', value: 'bbr', state: 'present' }
    - { name: 'net.ipv4.tcp_fin_timeout', value: '35', state: 'present' }

# Haproxy configuration management
- name: Generate /etc/haproxy/haproxy.cfg file | Internal Proxy Servers Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: int_haproxy.j2
    dest: /etc/haproxy/haproxy.cfg
    mode: 0644
    validate: /usr/sbin/haproxy -c -f %s
  register: haproxy_int_changed
  when: "'proxy_internal' in group_names"
  notify: Reload_haproxy

- name: Generate /etc/haproxy/haproxy.cfg file | Public Proxy Servers Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: pub_haproxy.j2
    dest: /etc/haproxy/haproxy.cfg
    mode: 0644
    validate: /usr/sbin/haproxy -c -f %s
  register: haproxy_pub_changed
  when: "'proxy_public' in group_names"
  notify: Reload_haproxy

# Keepalived configuration management
- name: Generate /etc/keepalived/keepalived.conf file | Proxy Servers Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: keepalived.j2
    dest: /etc/keepalived/keepalived.conf
    mode: 0644
    validate: /usr/sbin/keepalived -t -f %s
  register: keepalived_changed
  notify: Reload_keepalived

# nftables configuration management
- name: Generate /etc/nftables.conf file | Proxy Servers Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: pub_nftables.j2
    dest: /etc/nftables.conf
    validate: nft --check --file %s
    mode: 0655
  notify: Reload_nftables
  register: nftables_int_changed
  when: "'proxy_public' in group_names"

- name: Generate /etc/nftables.conf file | Proxy Servers Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: int_nftables.j2
    dest: /etc/nftables.conf
    validate: nft --check --file %s
    mode: 0655
  notify: Reload_nftables
  register: nftables_pub_changed
  when: "'proxy_internal' in group_names"

# Chrony config
- name: Generate /etc/chrony/chrony.conf file | NTP Client Debian All
  ansible.builtin.template:
    remote_src: false
    backup: true
    src: chrony.j2
    dest: /etc/chrony/chrony.conf
    mode: 0655
  notify: Restart_chrony

